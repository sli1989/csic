---
title: 奖励
date: 2021-01-01 01:00:00
tags: 研究成果
categories: 
keywords: 
description: 
photos: [
  ["/images/banner.jpg"]
] 
---

<p align="center">研究成果</p>

<!--more-->

1. 中国自动化学会科技进步奖；二等奖；2020年（丁芝侠，排名第三）
2. 中国石油和化学工业联合会科学技术奖；三等奖；2020年（丁芝侠，排名第四）
3. 武汉工程大学青年岗位能手；2018年（丁芝侠）


