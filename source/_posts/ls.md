---
title: 李赛
date: 2019-01-01 01:00:00
tags: 研究团队
categories: 
keywords: 
description: 
photos: [
  ["/images/banner.jpg"]
] 
---

<p align="center">研究团队</p>

<!--more-->

![avatar](../images/people/ls.png)

**职称**：讲师

**电子邮件**：sli@wit.edu.cn

**个人简历**

2018年12月毕业于华中科技大学，获控制科学与工程专业博士学位。

近年来参与指导学生参加“互联网+”大学生创新创业大赛、全国大学生数学建模竞赛、全国大学生电子设计竞赛、中国工程机器人大赛等，获省级以上奖励。近年来，作为项目负责人主持湖北省教育厅科研计划项目和武汉市专项计划项目，在 Reliability Engineering and System Safety、Expert Systems with Applications 等国际学术期刊上发表SCI检索论文2篇，在国际学术会议上发表EI检索论文8篇。

ORCID ID：[0000-0003-0253-843X](https://orcid.org/0000-0003-0253-843X)

**研究方向**

- 机器学习理论
- 辨识、控制与优化理论
- 动态系统的故障诊断、故障预报与健康管理
