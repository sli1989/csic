---
title: 论文
date: 2022-3-4 01:00:00
tags: 研究成果
categories: 
keywords: 
description: 
photos: [
  ["/images/banner.jpg"]
] 
---

<p align="center">研究成果</p>

<!--more-->

# 2022

1. Ding Z, Chen C, Wen S, et al. Lag projective synchronization of nonidentical fractional delayed memristive neural networks[J]. Neurocomputing, 469 (2022) 138–150. DOI: 10.1016/J.NEUCOM.2021.10.061 （二区）

# 2021

1. Ding Z, Zhang H, Zeng Z, Le Yang, Sai Li. Global Dissipativity and Quasi-Mittag-Leffler Synchronization of Fractional-Order Discontinuous Complex-Valued Neural Networks[J]. IEEE Transactions on Neural Networks and Learning Systems, 2021. DOI: https://doi.org/10.1109/TNNLS.2021.3119647 （一区）
1. Sai Li, Huajing Fang, and Bing Shi. Remaining Useful Life Estimation of Lithium-Ion Battery Based on Interacting Multiple Model Particle Filter and Support Vector Regression. Reliability Engineering & System Safety 210 (June 2021): 107542. doi:10.1016/j.ress.2021.107542.（一区）
3. Guan Wang, Rui Jiao, Ting Su, Zhixia Ding*, Sai Li, Liheng Wang. Finite-time Synchronization of Fractional-order Delayed Memristive Neural Networks by Discontinuous Control Scheme[C]//2021 International Conference on Neuromorphic Computing (ICNC). IEEE, 2021: 1-7. DOI: 10.1109/ICNC52316.2021.9607939 （EI）
4. Hailang Yang, Jianhao Wang, Shali Huang, Zhixia Ding*. Leader-following Consensus of Fractional-order Multi-agent Systems with Feedback Control Protocol[C]//2021 International Conference on Neuromorphic Computing (ICNC). IEEE, 2021: 22-27. DOI: 10.1109/ICNC52316.2021.9608991
5. Le Yang, Zhixia Ding*, Hongfei Liu, Yanyang Xu, Ting Su. Memristor-based Brain-like Reconfigurable Neuromorphic System[C]//2021 International Conference on Neuromorphic Computing (ICNC). IEEE, 2021: 292-296.  DOI: 10.1109/ICNC52316.2021.9608461 （EI）
1. Sai Li, Rui Jiao, Zhixia Ding, Liheng Wang, and Xuan Ye. Bearing Fault Diagnosis Using Support Vector Classifier Based on Sine Cosine Algorithm. 2021 33rd Chinese Control and Decision Conference (CCDC), 2021, pp. 7100-7105, doi: 10.1109/CCDC52312.2021.9602305. （EI）

# 2020


7. H. Zhang, Z. Ding, Z. Zeng, " Adaptive tracking synchronization for coupled reaction–diffusion neural networks with parameter mismatches, " Neural Networks, 2020, 124 :146-157, doi: 10.1016/j.neunet.2019.12.025. （一区）
1. C. Chen, Z. Ding*, S. Li, L. Wang, " Finite-time Mittag–Leffler synchronization of fractional-order delayed memristive neural networks with parameters uncertainty and discontinuous activation functions, " Chinese Physics B, 2020, 29 (4）: 040202. doi: 10.1088/1674-1056/ab7803. (三区)
24. Le Yang, Zhigang Zeng, Zhansong Ma and Wenqi Shan, A memristive dual-slope A/D converter, International Journal of Circuit Theory and Applications, 2020, 48(1): 42-55. (三区)
25. Le Yang, Zhigang Zeng and Yi Huang, An associative-memory-based reconfigurable memristive neuromorphic system with synchronous weight training,IEEE Transactions on Cognitive and Developmental Systems, 2020, 12(3): 529-540. (三区)
8. 丁芝侠，陈冲，胡蝶，“分数阶时滞基因调控网络一致稳定性分析，“ 华中科技大学学报（自然科学版），2020, 48(12):6.doi: 10.13245/j.hust.201205. （中文核心）
9. Y. Yang, Z. Ding*, L. Yang, S. Li and L. Wang, "Leader-following consensus of fractional-order multi-agent systems under switching topologies," 2020 Chinese Automation Congress (CAC), 2020, pp. 4723-4728, doi: 10.1109/CAC51589.2020.9326744. （EI）

# 2019

10. Z. Ding, Z. Zeng*, H. Zhang, L. Wang, L. Wang, " New results on passivity of fractional-order uncertain neural networks, " Neurocomputing, 2019, pp. 51-59, doi: 10.1016/j.neucom.2019.03.042.（二区）
11. C. Chen, Z. Ding*, " Projective Synchronization of Nonidentical Fractional-Order Memristive Neural Networks, " Discrete Dynamics in Nature and Society, 2019, doi: 10.1155/2019/8743482. (三区)
23. Le Yang, Zhigang Zeng and Xinming Shi, A memristor-based neural network circuit with synchronous weight adjustment, Neurocomputing, 2019, 363:114-124. (二区)
12. 陈冲，胡蝶，丁芝侠*，“分数阶基因调控网络的拉格朗日稳定性，“武汉工程大学学报，2019，第002期, 184-189.doi: 10. 3969/j. issn. 1674⁃2869. 2019. 02. 016. 
13. C. Chen, Z. Ding*, S. Li and L. Wang, "Synchronization of fractional-order memristive neural networks with time delays," 2019 Chinese Automation Congress (CAC), 2019, pp. 2754-2759, doi: 10.1109/CAC48633.2019.8996193. （EI）
14. D. Hu, Z. Ding*, S. Li and L. Wang, "Finite-time synchronization of chaotic circuit systems with double memristors," 2019 Chinese Automation Congress (CAC), 2019, pp. 3300-3304, doi: 10.1109/CAC48633.2019.8997196. （EI）

# 2018

1. Zhixia Ding, Zhigang Zeng, Leimin Wang. Robust Finite-Time Stabilization of Fractional-Order Neural Networks With Discontinuous and Continuous Activation Functions Under Uncertainty, IEEE Transactions on Neural Networks and Learning Systems,2018, 29 (5) :1477 -1490. （一区）
1. Sai Li, Huajing Fang*, and Xiaoyong Liu, Parameter optimization of support vector regression based on sine cosine algorithm, Expert Systems with Applications, 2018, 91: 63-77 (二区)
22.  Le Yang, Zhigang Zeng and Shiping Wen, A full-function pavlov associative memory implementation with memristance changing circuit, Neurocomputing, vol. 272, pp. 513–519, 2018. (二区)
21.  Le Yang, Zhigang Zeng, Yi Huang and Shiping Wen, Memristor-based circuit implementations of recognition network and recall network with forgetting stages, IEEE Transactions on Cognitive and Developmental Systems, vol. 10, no. 4, pp. 1133–1142, 2018.(三区)
27. Xinming Shi, Zhigang Zeng, Le Yang and Yi Huang, Memristor-based circuit design for neuron with homeostatic plasticity, IEEE Transactions on Emerging Topics in Computational Intelligence, vol. 2, no. 5, pp. 359–370, 2018. 
16. Z. Ding, "Passivity Analysis of Fractional-order Neural Networks with Time-varying Parameter Uncertainties," 2018 Chinese Automation Congress (CAC), 2018, pp. 265-268, doi: 10.1109/CAC.2018.8623024. （EI）
26. Le Yang and Zhigang Zeng, A memristor-cmos hybrid circuit for classical conditioning reflex, International Conference on Information Science and Technology, 2018, pp. 257–261.（EI）

# 2016

1. Zhixia Ding, Yi Shen. Projective synchronization of nonidentical fractional-order neural networks based on sliding mode controller, Neural Networks ,2016, 76 : 97 -105.（一区）
18. Zhixia Ding, Yi Shen, Leimin Wang. Global Mittag-Leffler synchronization of fractional-order neural networks with discontinuous activations, Neural Networks, 2016, 73: 77 -85.（一区）
19. Zhixia Ding, Yi Shen. Global dissipativity of fractional-order neural networks with time delays and discontinuous activations, Neurocomputing, 2016, 196 :159 -166.（二区）

# 2015

1. Leimin Wang, Yi Shen, Zhixia Ding. Finite time stabilization of delayed neural networks, Neural Networks, 2015,70 :74 -80.（一区）
