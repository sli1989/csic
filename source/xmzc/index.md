---
title: 项目支持
date: 2022-02-22 22:22:22
---

1. 国家自然科学基金面上项目，分数阶不连续复值神经网络同步控制及其在保密通信中的应用研究（62176189）
2. 国家自然科学基金青年基金项目，基于不连续激励函数的分数阶忆阻神经网络控制方法研究（61703312）
3. 国家自然科学基金青年科学基金项目，基于忆阻的类脑可重构神经形态电路分析与设计 (62106181)  
4. 武汉市2019年专项人才计划项目
5. 湖北省教育厅科学技术研究项目——基于多模型估计的滚动轴承故障预测研究（Q20201509） 
6. 冶金工业过程系统科学湖北省重点实验室开放基金，分数阶忆阻神经网络分析与智能控制（Y202002）
7. 智能机器人湖北省重点实验室创新基金项目，忆阻类脑可重构神经形态系统的分析与设计 (HBIRL202109) 
8. 横向项目，便携式实训系统开发 
