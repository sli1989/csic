---
title: 招生信息
date: 2022-02-22 22:22:22
---

<h2 align="center">复杂系统与智能控制实验室招生启事</h2>

杂系统与智能控制实验室以“先进控制与智能自动化的理论、技术与应用”为总体研究方向，欢迎感兴趣的同学报考研究生，以及对科研感兴趣的本科生加入课题组，课题组每年招收3-6名研究生。 

一、招收条件

1. 专业方向：自动化、数学、计算机、物理等；
2. 有相关研究经验者优先考虑；
3. 具有较强的理论基础、实践能力、独立的科研能力，以及良好的团队合作精神。

二、申请材料

个人简历以及主要成绩介绍，简要说明未来两年学习计划。

三、申请方式

请将申请材料发送到邮箱：丁老师 （17070401@wit.edu.cn） , 杨老师 （leyangmail@163.com），李老师 （sli@wit.edu.cn）。
