#!/bin/bash
# ./fixNext.sh next_theme_dir
# for example put FixNext in hexo_dir and use: `FixNext/fixNext.sh themes/next`

# pull the lastest Next theme
# git clone https://github.com/theme-next/hexo-theme-next $1
# rm -rf themes/next
# git clone git@github.com:theme-next/hexo-theme-next.git themes/next
# git clone git@github.com:sli1989/hexo-theme-next.git themes/next

# git submodule update --remote --recursive
cd themes/acorn
git checkout master
# git config --global http.proxy http://127.0.0.1:8580
# git config --global --unset http.proxy
# 强制远程分支覆盖本地分支
git fetch --all
git reset --hard origin/master
git pull
cd ../../

git add .
git commit -m "update acorn" 
git push -u origin master 
